=== Advanced Ads – AdSense In-Feed ===
Contributors: webzunft
Tags: adsense, in-feed, infeed, ads
Requires at least: 4.5
Tested up to: 4.8
Stable tag: 1.0
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

Display AdSense In-feed ads between posts.

== Description ==

This plugin is an add-on for the also free [Advanced Ads](https://wordpress.org/plugins/advanced-ads/) ad management plugin.
It extends the plugin with a placement to target ads between posts on your homepage, archive pages, etc.

AdSense InFeed ads are a dedicated ad type to monetize overview pages with a highly engaging layout and specialized options.

Advanced Ads was developed to place, manage and test ads on pages to increase revenue and usability. 

The plugin comes with plenty of features, e.g., 

* support for all AdSense types like link units, responsive, In-feed and In-article
* AdSense term violation checks
* Page-Level ads

[Full feature list](https://wpadvancedads.com/features/)

Pro features include

* Ads on AMP pages
* Ads based on specific browser widths
* showing an alternative ad to ad block users
* click fraud monitoring
* position tests
* support for caching plugins

== Screenshots ==

== Changelog ==

= 1.0 =

* first version