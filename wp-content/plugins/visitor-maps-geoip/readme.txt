=== Visitor Maps Geolocation Addon ===
Contributors: Mike Challis
Author URI: http://www.642weather.com/weather/scripts.php
Donate link: https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=V3BPEZ9WGYEYG
Tags: plugin, plugins, users, visitors, visitor, whos online, map, maps, geolocation, location, country, statistics, stats, widget, sidebar, admin, dashboard
Requires at least: 2.8
Tested up to: 4.6.1
Stable tag: trunk

Adds the Geolocation and Maps feature to Visitor Maps version 1.5.8.8 or higher.

== Description ==

This addon is required before you can enable the Geolocation features in Visitor Maps Wordpress Plugin. After installing this plugin, go to the Visitor Maps Options menu to click to "Install" the Maxmind GeoLite City Database, then click "Enable Geolocation" and click Update Options. 

= Help Keep This Plugin Free =

If you find this plugin useful to you, please consider [__making a small donation__](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=V3BPEZ9WGYEYG) to help contribute to my time invested and to further development. Thanks for your kind support! - [__Mike Challis__](http://profiles.wordpress.org/users/MikeChallis/)



Requirements/Restrictions:
-------------------------
 * Works with Wordpress 2.8+. (Wordpress 4.3+ is highly recommended)
 * Visitor Maps version 1.5.8.8 or higher
 * PHP5
 * 30 megs of server space(with geolocation enabled)
 * PHP register_globals and safe_mode should be set to "Off"

Credits:
-------------------------
* Programmed by [Mike Challis](http://profiles.wordpress.org/mikechallis/), [Contact Mike Challis](http://www.642weather.com/weather/contact_us.php)
* Uses GeoLiteCity data created by MaxMind, available from [http://www.maxmind.com](http://www.maxmind.com)


== Installation ==

1. To install, go to your WordPress admin page for Plugins, then click 'Add New', then on the 'Add Plugins' page, click the "Upload Plugin" button, select the visitor-maps-geoip.zip you previously downloaded, then click "Install Now". Or you can just unzip and FTP upload the /visitor-maps-geoip/ folder to the /wp-content/plugins/ directory. 

2. After installation be sure to activate the plugin. Next, go to the Visitor Maps Options menu to click to "Install" the Maxmind GeoLite City Database, then click "Enable Geolocation" and click Update Options



== Changelog ==

= 1.0.1 =
- (22 Oct 2016) - fix error: get_settings at line 15: function get_settings deprecated

= 1.0 =
- (29 Aug 2015) - first version

