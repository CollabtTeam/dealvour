<?php
/*
Plugin Name: Visitor Maps Geolocation Addon
Version: 1.0.1
Plugin URI: http://www.642weather.com/weather/scripts-wordpress-visitor-maps.php
Description: Adds the Geolocation and Maps feature to Visitor Maps version 1.5.8.8 or higher
Author: Mike Challis
Author URI: http://www.642weather.com/weather/scripts.php
*/
/*  Copyright (C) 2016 Mike Challis  (http://www.642weather.com/weather/contact_us.php)

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

class VisitorMapsGeolocation {

     /**
     * Add actions to load the plugin
     */
    public function __construct() {

       // visitor_maps init plugin
     add_action('init', array(&$this, 'visitor_maps_init'));
     add_action('visitor_maps_geoip', array(&$this, 'adminDisplayForm'));

   }

 function visitor_maps_init() {

 if (function_exists('load_plugin_textdomain')) {
      load_plugin_textdomain('visitor-maps-geoip', false, 'visitor-maps-geoip/languages' );
 }

} // end function visitor_maps_init

    /**
     * Show the geoip ettings in Visitor maps
     */
    function adminDisplayForm($visitor_maps_opt) {
       //
      echo '<strong>'.  __('Uses GeoLiteCity data created by MaxMind, available from http://www.maxmind.com', 'visitor-maps') .'</strong><br /><br />';
      if ( !is_file(WP_CONTENT_DIR .'/visitor-maps-geoip/GeoLiteCity.dat') ) {
              echo '<div id="update-nag" class="error"><p><strong>'.  __('The Maxmind GeoLiteCity database is not yet installed.', 'visitor-maps'). ' <a style="color:red" href="' . wp_nonce_url(admin_url( 'plugins.php?page=visitor-maps/visitor-maps.php' ),'visitor-maps-geo_update') . '&amp;do_geo=1">'. __('Install Now', 'visitor-maps'). '</a></strong></p></div>';
              echo '<span style="background-color:#FFE991; padding:4px;"><strong>'.  __('The Maxmind GeoLiteCity database is not yet installed.', 'visitor-maps'). ' <a style="color:red" href="' . wp_nonce_url(admin_url( 'plugins.php?page=visitor-maps/visitor-maps.php' ),'visitor-maps-geo_update') . '&amp;do_geo=1">'. __('Install Now', 'visitor-maps'). '</a></strong></span>';
      } else if (!$visitor_maps_opt['enable_location_plugin']) {
              echo '<span style="background-color:#FFE991; padding:4px;"><strong>'.  __('The Maxmind GeoLiteCity database is installed but not enabled (check the setting below).', 'visitor-maps'). '</strong></span>';
      } else {
             echo '<span style="background-color:#99CC99; padding:4px;"><strong>'.  __('The Maxmind GeoLiteCity database is installed and enabled.', 'visitor-maps'). '</strong></span>';
      }
      ?>
       <br />
            <br />
      <input name="visitor_maps_enable_location_plugin" id="visitor_maps_enable_location_plugin" type="checkbox" <?php if( $visitor_maps_opt['enable_location_plugin'] ) echo 'checked="checked"'; ?> />
      <label for="visitor_maps_enable_location_plugin"><?php echo __('Enable geolocation.', 'visitor-maps'); ?></label>
      <?php if( $visitor_maps_opt['enable_location_plugin'] && is_file(WP_CONTENT_DIR .'/visitor-maps-geoip/GeoLiteCity.dat')) echo ' <a href="' . wp_nonce_url(admin_url( 'plugins.php?page=visitor-maps/visitor-maps.php' ),'visitor-maps-geo_update') . '&amp;do_geo=1">'. __('Update Now', 'visitor-maps'). '</a>';?>
      <br />

      <?php
    }

    /**
     * Show the geoip ettings in Visitor maps
     */
    function viewDisplayForm($geoip_old, $geoip_days_ago) {

    echo '<p>'.esc_html( __( 'Uses GeoLiteCity data created by MaxMind, available from http://www.maxmind.com', 'visitor-maps' ) ).'<br />';
                               if( $geoip_old ){
                                   echo '<span style="color:red">'.
                                   //sprintf( __('The GeoLiteCity data was last updated on %1$s (%2$d days ago)','visitor-maps'),date($visitor_maps_opt['geoip_date_format'], $geoip_file_time),$geoip_days_ago).' '.
                                   sprintf( __('The GeoLiteCity data was last updated %d days ago','visitor-maps'),$geoip_days_ago).' '.

                                   esc_html( __( 'an update is available', 'visitor-maps' ) ).',
                                   <a href="' . wp_nonce_url(admin_url( 'plugins.php?page=visitor-maps/visitor-maps.php' ),'visitor-maps-geo_update') . '&do_geo=1">'.esc_html( __( 'click here to update', 'visitor-maps' ) ).'</a></span>';
                               } else {
                                   //echo sprintf(__('The GeoLiteCity data was last updated on %1$s (%2$d days ago)','visitor-maps'),date($visitor_maps_opt['geoip_date_format'], $geoip_file_time),$geoip_days_ago);                                               ;
                                echo sprintf(__('The GeoLiteCity data was last updated %d days ago','visitor-maps'),$geoip_days_ago);                                               ;

                               }
                               echo '</p>';
    }

} // end class

$VisitorMapsGeolocation = new VisitorMapsGeolocation;

/**
 * Required to trigger Geo IP settings in visitor maps
 * @deprecated
 */
if(!function_exists('visitor_maps_geoip_admin')) { function visitor_maps_geoip_admin() {} }

