<div class="product-column">
    <div class="product-card" id="card-<?php the_ID(); ?>">
        <div class="card-content">
            <?php if (has_post_thumbnail()) : ?>
                <a href="<?php the_permalink(); ?>" class="product-img">
                    <?php the_post_thumbnail('product-card-thumbnails'); ?>
                </a>
            <?php endif; ?>
            <div class="card-details">
                <a target="_blank" href="<?php acf_the_url(get_field('offer_url')); ?>"
                   class="manufacturer"><?php acf_the_url_abbr(get_field('offer_url')); ?></a>
                   <p class="price">
                    <?php the_field('sale_price') ?>
                    <?php if (get_field('regular_price')): ?>
                        <sup><?php the_field('regular_price') ?></sup>
                    <?php endif; ?>
                </p>

                <p class="description">
                    <a href="<?php the_permalink(); ?>"><?php dv_smart_truncate(get_the_title(), 55); ?></a>
                </p>

                <?php if (get_field('subtitle')) : ?>
                    <p class="discount"><?php the_field('subtitle'); ?></p>
                <?php endif; ?>

            </div>
        </div>
        <div class="card-buttons">
            <a class="share-popup-button" href="#">Share</a>
            <?php if (get_field('coupon_code')) : ?>
                <a href="<?php the_permalink(); ?>"><?php _e('Get Code', 'html5blank'); ?></a>
            <?php else: ?>
                <a href="<?php the_permalink(); ?>"><?php _e('View now', 'html5blank'); ?></a>
            <?php endif; ?>

        </div>
        <div class="ui share-section-popup flowing popup bottom right transition hidden">
            <h4 class="share-title">Share</h4>
            <?php do_action('render_share_icon', array('url' => get_the_permalink(), 'title' => get_the_title())); ?>
        </div>
    </div>
</div>
