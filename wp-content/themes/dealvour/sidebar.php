<div class="sixteen wide tablet five wide computer column ">

    <div class="right-sidebar">
        <div class="sidebar-widget">
            <?php if (is_singular('deals')): ?>
                <div class="sidebar-deals-block-wrap">
                    <?php get_template_part('partials/sidebar-deals'); ?>
                </div>
                <?php if (is_active_sidebar('sidebar_widgets_deal_bottom')) : ?>
                    <?php dynamic_sidebar('sidebar_widgets_deal_bottom'); ?>
                <?php endif; ?>
            <?php elseif (is_search()) : ?>
                <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar_search')) ?>
            <?php else : ?>
                <?php if (!function_exists('dynamic_sidebar') || !dynamic_sidebar('sidebar_widgets')) ?>
            <?php endif; ?>
        </div>
    </div>
</div>
