<?php global $frontend; ?>
<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
	
	<meta name="verifyownership" 
 content="f0fb0f06d4333e700db303ee0f597caa"/>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">

    <title><?php wp_title(''); ?></title>
    <meta name="description" content="<?php bloginfo('description'); ?>">
    <meta name="author" content="">

    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

    <?php wp_head(); ?>
    <script type="text/javascript">
        <?php if (is_tax('deals_cat') || is_singular('deals') || is_search()): ?>
            var currentPage = 'deal';
        <?php elseif (is_singular('post')):?>
            var currentPage = 'news';
        <?php else: ?>
            var currentPage = null;
        <?php endif;?>

    </script>
</head>

<!-- #BODY -->

<body <?php body_class(); ?>>
<div class="main-wrapper">
    <?php get_template_part('partials/mobile-nav');?>
    <header>
        <div class="header-wrapper">
            <div class="ui container">
                <div class="main-menu-wrapper">
                    <?php get_search_form(); ?>
                    <div class="main-logo menu-item">
                        <a href="<?php echo home_url(); ?>">
                            <?php if (function_exists('the_custom_logo')) {
                                the_custom_logo();
                            } ?>
                        </a>
                    </div>
                    <div class="menu-nav menu-item">
                        <?php $frontend::menu('header-menu', 'header-menu-wrapper') ?>
                    </div>
                </div>
            </div>
        </div>
    </header>