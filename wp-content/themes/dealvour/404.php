<?php get_header(); ?>

<main role="main">
    <div class="product-content-wrapper">
        <?php get_template_part('partials/advert-after-header') ?>
        <div class="product-news-content">
            <div class="ui grid container">
                <div class="wrapper-404 search-res-block search-filter">

                    <h1><?php _e( 'Oops!', 'html5blank' ); ?><strong><?php _e( '404', 'html5blank' ); ?></strong></h1>

                    <p><?php _e( 'Sorry, we could not find that page. It no longer exists or has moved to a new location.!', 'html5blank' ); ?></p>
                    <p><?php _e( 'Try searching again or check out our most popular deals on', 'html5blank' ); ?>
                        <a href="<?php echo home_url(); ?>"><?php _e( 'Dealvour’s home page.', 'html5blank' ); ?></a>
                    </p>


                </div>


            </div>
        </div>
        <?php get_template_part('partials/advert-before-footer') ?>
    </div>

</main>

<?php get_footer(); ?>
