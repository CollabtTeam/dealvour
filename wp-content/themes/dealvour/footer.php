<?php global $frontend; ?>

<footer>
    <div class="ui container">
        <div class="footer-content">
            <div class="footer-column">
                <a href="<?php echo home_url(); ?>">
                    <?php if ( function_exists( 'the_custom_logo' ) ) { the_custom_logo(); }?>
                </a>
            </div>
            <div class="footer-column">
                <div class="footer-nav">
                    <div class="column">
                        <h3 class="footer-caption"><?php _e('Helpful links', 'html5blank');?></h3>
                        <?php $frontend::menu('footer-menu', 'footer-nav-list') ?>
                    </div>
                    <div class="column">
                        <h3 class="footer-caption"><?php _e('Find us on','html5blank');?> </h3>
                        <ul class="footer-social-nav">
                            <?php if (get_theme_mod('facebook_link')):?>
                                <li><a href="<?php echo get_theme_mod('facebook_link'); ?>"><i class="facebook f icon"></i></a></li>
                            <?php endif ?>
                            <?php if (get_theme_mod('instagram_link')):?>
                                <li><a href="<?php echo get_theme_mod('instagram_link'); ?>"><i class="instagram icon"></i></a></li>
                            <?php endif ?>
                            <?php if (get_theme_mod('pinterest_link')):?>
                                <li><a href="<?php echo get_theme_mod('pinterest_link'); ?>"><i class="pinterest icon"></i></a></li>
                            <?php endif ?>
                            <?php if (get_theme_mod('twitter_link')):?>
                                <li><a href="<?php echo get_theme_mod('twitter_link'); ?>"><i class="twitter icon"></i></a></li>
                            <?php endif ?>
                        </ul>
                    </div>
                </div>
                <div class="copyright-text"><?php echo get_theme_mod('footer_copyright');?></div>
            </div>
        </div>
    </div>
</footer>
</div>
<?php wp_footer(); ?>

</body>

</html>
