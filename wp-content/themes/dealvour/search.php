<?php get_header(); ?>

<?php get_header(); ?>

<main role="main">
    <div class="product-content-wrapper">
        <?php get_template_part('partials/advert-after-header') ?>
        <div class="product-news-content">
            <div class="ui grid container">
                <div class="sixteen wide column">
                    <div class="search-res-block">
                        <?php _e('Search Results: ', 'html5blank');
                        echo sprintf('"%s"', get_search_query()); ?>
                    </div>
                </div>
            </div>
            <?php if (is_search()) : ?>
                <div class="search-mobile-block" style="display: none;">
                    <div class="ui grid container">
                        <div class="sixteen wide column">
                            <?php echo do_shortcode('[widget]')?>
                        </div>
                    </div>
                </div>
            <?php endif ; ?>
            <?php get_template_part('partials/advert-search-top') ?>

            <div class="ui grid container">

                <div class="sixteen wide tablet eleven wide computer column">
                    <div class="serach-results-wrapper">
                        <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                            <section class="article-news hide-comment search-products-block"
                            id="post-<?php the_ID(); ?>">
                            <div class="mobile-manufacturer">
                                <a target="_blank" href="<?php acf_the_url(get_field('offer_url')); ?>"
                                 class="manufacturer" style="display: none;"><?php acf_the_url_abbr(get_field('offer_url')); ?></a>
                             </div>
                             <?php if (has_post_thumbnail()) : ?>
                                <a class="search-img" href="<?php the_permalink(); ?>"
                                 title="<?php the_title(); ?>">
                                 <?php if (is_expired(get_field('offer_expiration_date'))): ?>
                                    <span><?php _e('expired', 'html5blank'); ?> </span>
                                <?php endif; ?>

                                <?php the_post_thumbnail('search-thumbnails'); ?>
                            </a>
                        <?php endif; ?>
                        <div class="search-result-content">
                            <a target="_blank" href="<?php acf_the_url(get_field('offer_url')); ?>"
                             class="manufacturer"><?php acf_the_url_abbr(get_field('offer_url')); ?></a>
                             <h3 class="title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h3>
                             <div class="price-block">
                                <?php if (get_field('sale_price')): ?>
                                    <p class="price">
                                        <?php the_field('sale_price') ?>
                                    </p>
                                <?php endif; ?>
                                <p class="old-price">
                                    <?php if (get_field('regular_price')): ?>
                                        <?php _e('', 'html5blank'); ?>
                                        <span><?php the_field('regular_price') ?></span>
                                    <?php endif; ?>
                                </p>
                            </div>
                            <div class="sale-details">
                                <?php if (get_field('free_shipping')): ?>
                                    <p class="free-shipping">
                                        <?php the_freeshipping(get_field('free_shipping'))?>
                                    </p>
                                <?php endif; ?>

                                <?php if (get_field('coupon_code')) : ?>
                                    <p class="code"><?php _e('Deal with Coupon Code: ', 'html5blank'); ?>
                                        "<?php the_field('coupon_code'); ?>"</p>
                                    <?php endif; ?>
                                </div>
                            </div>


                        </section>

                    <?php endwhile; endif; ?>

                    <?php get_template_part('short-pagination'); ?>

                    <?php wp_reset_query(); ?>
                </div>
            </div>
            <?php get_sidebar(); ?>
        </div>
    </div>
    <?php get_template_part('partials/advert-before-footer') ?>
</div>

</main>

<?php get_footer(); ?>

