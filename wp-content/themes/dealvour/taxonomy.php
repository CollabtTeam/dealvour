<?php get_header(); ?>

    <main role="main">
        <div class="product-content-wrapper">
            <?php get_template_part('partials/advert-after-header') ?>
            <div class="content-wrapper">
                <div class="products-section">
                    <div class="ui container">
                        <div class="products-wrapper">
                            <div class="title-wrapper">
                                <h2 class="title"><?php single_term_title(); ?></h2>
                            </div>
                            <?php $index = 1; ?>
                            <div class="products-card-wrapper ">
                                <?php while (have_posts()): the_post();
                                    include "loop-deal.php";
                                    do_action('after_deal_loop_content', $index, $wp_query->post_count !== $index);
                                    $index++;
                                endwhile; ?>
                            </div>
                        </div>
                        <?php get_template_part('short-pagination'); ?>

                    </div>
                </div>
            </div>

            <?php get_template_part('partials/advert-before-footer') ?>
        </div>

    </main>

<?php get_footer(); ?>