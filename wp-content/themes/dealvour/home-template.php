<?php /* Template Name: Home Template */
get_header(); ?>

<main>
    <?php get_template_part('partials/advert-after-header') ?>
    <?php $paged = get_query_var('page') ? get_query_var('page') : 1; ?>


    <div class="content-wrapper">
        <?php if ($paged === 1): ?>
            <div class="ui grid container">
                <?php
                if (get_theme_mod('show_featured_slider')):
                    $query_args = array(
                        'post_status' => 'publish',
                        'post_type' => 'deals',
                        'posts_per_page' => get_theme_mod('enable_featured_slider') ? -1 : 4,
                        'orderby' => get_theme_mod('random_order_deals') ? 'rand' : 'DESC',
                        'meta_key' => 'featured',
                        'meta_value' => 'yes'
                    );
                    $random_class = get_theme_mod('random_order_deals') ? 'owl-random' : '';
                    query_posts($query_args);

                    if (have_posts()): ?>

                    <div class="products-section">
                        <div class="ui container">
                            <div class="products-wrapper">
                                <div class="title-wrapper"><h2 class="title"><?php _e('Featured deals', 'html5blank') ?></h2></div>
                                <div class="owl-carousel owl-theme JS-homePage-carousel <?php echo $random_class; ?>">
                                    <?php while (have_posts()): the_post(); ?>
                                        <?php include "loop-deal.php"; ?>
                                    <?php endwhile;
                                    wp_reset_postdata();
                                    wp_reset_query(); ?>
                                </div>

                            </div>
                        </div>
                    </div>
                <?php endif; ?>
            <?php endif; ?>

            <?php
            $query_args = array(
                'post_status' => 'publish',
                'post_type' => 'post',
                'posts_per_page' => 4,
                'order' => 'DESC',
                'meta_key' => 'featured',
                'meta_value' => 'yes'
            );
            query_posts($query_args);
            if (have_posts()):?>

            <div class="products-section">
                <div class="ui container">
                    <div class="products-wrapper">
                        <div class="title-wrapper"><h2 class="title"><?php _e('Trending Articles', 'html5blank') ?></h2></div>
                        <div class="featured-posts-container">
                            <?php while (have_posts()): the_post(); ?>
                                <div class="trending-column">
                                    <article class="tranding-news">
                                        <?php if (has_post_thumbnail()): ?>
                                            <a href="<?php the_permalink() ?>">
                                                <?php the_post_thumbnail('trending-thumbnails') ?>
                                            </a>
                                        <?php endif; ?>
                                        <h3 class="featured-title">
                                            <a href="<?php the_permalink() ?>">
                                                <?php the_title(); ?>
                                            </a></h3>
                                            <!-- <?php the_excerpt(); ?> -->
                                            <a class="continue-read"
                                            href="<?php the_permalink(); ?>"><?php _e('Continue Reading', 'html5blank'); ?></a>
                                        </article>
                                    </div>
                                <?php endwhile;
                                wp_reset_postdata();
                                wp_reset_query(); ?>


                            </div>
                        </div>
                        <?php get_template_part('partials/advert-trand-article') ?>
                    </div>
                </div>
            <?php endif; ?>
        <?php endif; ?>

        <?php
        $paged = get_query_var('page') ? get_query_var('page') : 1;

        $query_args = array(
            'post_status' => 'publish',
            'paged' => $paged,
            'post_type' => array('deals'),
            'posts_per_page' => (int)get_theme_mod('today_deals_per_page')

        );
        query_posts($query_args); ?>
        <?php if (have_posts()): ?>

            <div class="products-section">
                <div class="ui container">
                    <div class="products-wrapper">
                        <div class="title-wrapper"><h2 class="title"><?php _e('Today\'s Hot Deals & Coupons', 'html5blank') ?></h2></div>

                        <div class="products-card-wrapper">
                            <?php $index = 1; ?>
                            <?php while (have_posts()): the_post();
                            include "loop-deal.php";
                            do_action('after_top_deal_loop_content', $index, $wp_query->post_count !== $index);
                            $index++;
                        endwhile;
                        wp_reset_postdata(); ?>
                    </div>
                    <?php get_template_part('short-pagination'); ?>
                </div>
            </div>
        <?php endif; ?>

    </div>
    <?php wp_reset_query(); ?>

</div>

</main>
<?php get_template_part('partials/advert-before-footer') ?>

<?php get_footer(); ?>

