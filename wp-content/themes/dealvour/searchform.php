<div class="search-menu menu-item">
    <form method="get" action="<?php echo home_url(); ?>" role="search">
        <div class="ui icon input">
            <input type="text" name="s" placeholder="<?php _e( 'Search Stores, Deals & Coupons.', 'html5blank' ); ?>">
            <input type="hidden" name="post_type" value="deals">
            <i class="search link icon"></i>
            <button class="search-submit"   type="submit" role="button"><?php _e( 'Search', 'html5blank' ); ?></button>
        </div>

    </form>
</div>