<?php

if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails' );
	add_image_size( 'large', 820, 600, false ); // Large Thumbnail
	add_image_size( 'medium', 340, 240, true ); // Medium Thumbnail
	add_image_size( 'small', 80, 80, false ); // Small Thumbnail
	add_image_size( 'news-widget', 200, 110, true ); // News widget
	add_image_size( 'product-card-thumbnails', 184, 184, false ); // Product card
	add_image_size( 'trending-thumbnails', 270, 200, true ); // Trending Articles
	add_image_size( 'trending-thumbnails-mobile', 451, 335, true ); // Trending Articles mobile
	add_image_size( 'search-thumbnails', 125, 135, false ); // Search thumbnails
	add_image_size( 'featured-thumb', 460, 400, true ); // Featured top block
	add_image_size( 'blog-thumbnails', 767, 300, true ); // BLOG post thumbnail

}

function the_blog_class( $index ) {
	/* if ($index == 1) {
		 $class = 'last-post';
	 } else if ($index % 2 == 0) {
		 $class = 'odd-post';
	 } else {
		 $class = 'even-post';
	 }*/
	$class = 'last-post';
	echo $class;
}

function dv_ssba_add_button_filter() {
	add_action( 'render_share_icon', 'dv_single_page_show_share_icons' );
	remove_filter( 'the_content', 'show_share_buttons', 10 );
	remove_action( 'wp_head', 'ssba_add_button_filter', 99 );
}


add_action( 'init', 'dv_ssba_add_button_filter', 100 );

function dv_single_page_show_share_icons( $atts = false ) {
	if ( ! $atts ) {
		echo do_shortcode( '[ssba]' );

	} else {
		echo do_shortcode( '[ssba url=' . $atts["url"] . ' title=' . $atts["title"] . ']' );
	}
}

add_action( 'init', 'dv_init_blog_features' );

function dv_init_blog_features() {
	new FeaturedPosts();
}

if ( ! class_exists( 'FeaturedPosts' ) ) {

	class FeaturedPosts {

		public $metakey = 'featured';
		public $nonce = 'blog_post_class_nonce';

		//constarctor
		public function __construct() {

			global $pagenow, $typenow; //&& $typenow =='page'

			if ( is_admin() && $pagenow == 'edit.php' ) {
				add_filter( 'admin_footer', array( $this, 'insert_ajax_status_script' ) );
			}

			add_filter( 'manage_post_posts_columns', array( $this, 'dw_posts_table_column' ) );
			add_action( 'manage_post_posts_custom_column', array( $this, 'dv_featured_column' ), 10, 2 );

			//bulk action
			add_filter( 'bulk_actions-edit-post', array( $this, 'dv_bulk_featured' ) );
			add_action( 'handle_bulk_actions-edit-post', array( $this, 'dv_handle_bulk_featured' ), 10, 3 );

			add_filter( 'views_edit-post', array( $this, 'dv_filter_featured' ) );
			add_action( 'pre_get_posts', array( $this, 'dv_filter_post_featured' ) );

			//ajax function
			add_action( 'wp_ajax_change_post_featured_status', array( $this, 'ajax_change_featured_status' ) );

			add_action( 'add_meta_boxes', array( $this, 'dv_add_post_meta_boxes' ), 10, 1 );
			add_action( 'save_post', array( $this, 'save_post_meta_boxes' ), 10, 2 );

		}

		public function dv_add_post_meta_boxes() {

			add_meta_box(
				'featured',
				esc_html__( 'Featured' ),
				array( $this, 'dv_render_metaboxes' ),
				'post',
				'side',
				'high'
			);

		}

		public function dv_render_metaboxes( $post ) {
			wp_nonce_field( basename( __FILE__ ), $this->nonce );
			$featured = get_post_meta( $post->ID, $this->metakey, true ); ?>
            <p>
                <label for="dv-<?php echo $this->metakey; ?>">
                    <input type="checkbox" value="yes" name="<?php echo $this->metakey ?>"
                           id="dv-<?php echo $this->metakey; ?>" <?php if ( $featured === 'yes' ) {
						echo "checked ";
					} ?> />
					<?php _e( "Featured", 'example' ); ?>
                </label>
            </p>
		<?php }

		public function save_post_meta_boxes( $post_id, $post ) {
			if ( ! isset( $_POST[ $this->nonce ] ) || ! wp_verify_nonce( $_POST[ $this->nonce ], basename( __FILE__ ) ) ) {
				return $post_id;
			}


			$post_type = get_post_type_object( $post->post_type );

			if ( ! current_user_can( $post_type->cap->edit_post, $post_id ) ) {
				return $post_id;
			}

			$new_meta_value = ( isset( $_POST[ $this->metakey ] ) ? $_POST[ $this->metakey ] : '' );

			$meta_key = $this->metakey;

			$meta_value = get_post_meta( $post_id, $meta_key, true );

			if ( $new_meta_value && '' == $meta_value ) {
				add_post_meta( $post_id, $meta_key, $new_meta_value, true );
			} elseif ( $new_meta_value && $new_meta_value != $meta_value ) {
				update_post_meta( $post_id, $meta_key, $new_meta_value );
			} elseif ( '' == $new_meta_value && $meta_value ) {
				delete_post_meta( $post_id, $meta_key, $meta_value );
			}

		}


		public function change_post_status( $post_id, $status ) {
			$current_post                = get_post( $post_id, 'ARRAY_A' );
			$current_post['post_status'] = $status;
			wp_update_post( $current_post );
		}

		public function dv_handle_bulk_featured( $redirect_to, $doaction, $post_ids ) {
			if ( ( $doaction != 'featured' ) && ( $doaction != 'unfeatured' ) && ( $doaction != 'draft' ) && ( $doaction != 'publish' ) ) {
				return;
			}

			if ( ( $doaction == 'featured' ) || ( $doaction == 'unfeatured' ) ) {

				switch ( $doaction ) {
					case 'featured':
						$value      = 'yes';
						$meta_value = $this->metakey;
						break;
					case 'unfeatured':
						$value      = 'no';
						$meta_value = $this->metakey;
						break;
					default:
						break;
				}
				foreach ( $post_ids as $id ) {
					update_post_meta( $id, $meta_value, $value );
				}
			}


			if ( ( $doaction == 'draft' ) || ( $doaction == 'publish' ) ) {

				foreach ( $post_ids as $id ) {
					$this->change_post_status( $id, $doaction );
				}
			}

			return $redirect_to;
		}

		public function dv_bulk_featured( $bulk_actions ) {
			$bulk_actions['featured']   = __( 'Featured', 'html5blank' );
			$bulk_actions['unfeatured'] = __( 'Unfeatured', 'html5blank' );
			$bulk_actions['draft']      = __( 'Draft', 'html5blank' );
			$bulk_actions['publish']    = __( 'Publish', 'html5blank' );

			return $bulk_actions;
		}

		public function dv_filter_post_featured( $query ) {
			if ( is_admin() && isset( $query->query['post_type'] ) && $query->query['post_type'] == 'post' ) {
				if ( $query->get( 'orderby' ) == 'menu_order title' ) {
					$query->set( 'orderby', 'date' );
					$query->set( 'order', 'desc' );
				}
				if ( isset( $_GET['meta_key'] ) && ( $_GET['meta_key'] == 'featured' ) ) {
					$query->set( 'meta_key', 'featured' );
					$query->set( 'meta_value', 'yes' );
				}
			}

		}

		public function dv_filter_featured( $views ) {

			$class_name = ( isset( $_GET['meta_key'] ) && $_GET['meta_key'] == 'featured' ) ? "current" : "";

			$args = array(
				'post_type'  => 'post',
				'meta_key'   => 'featured',
				'meta_value' => 'yes'
			);

			$result = new WP_Query( $args );

			$views['featured'] = sprintf(
				'<a  class="%s" href="%s">' . __( 'Featured' ) . ' <span class="count">(%d)</span></a>',
				$class_name,
				admin_url( 'edit.php?meta_key=featured&meta_value=yes' ),
				$result->found_posts
			);


			return $views;

		}


		public function ajax_change_featured_status() {

			$result       = '';
			$new_postmeta = '';
			$postid       = filter_input( INPUT_GET, 'post_id', FILTER_SANITIZE_NUMBER_INT );

			if ( ! isset( $postid ) ) {
				$result = 'something went wrong ...';
			}

			$old_postmeta = get_post_meta( $postid, $this->metakey, true );

			// if no status was saved, predefine the new status to no.
			if ( ! in_array( $old_postmeta, array( 'yes', 'no' ) ) ) {
				$old_postmeta = 'no';
			}

			$new_postmeta = ( $old_postmeta === 'yes' ) ?
				'no' : 'yes';

			if ( empty( $result ) ) {
				$result = 'change status to ' . $new_postmeta;
			}

			update_post_meta( $postid, $this->metakey, $new_postmeta );

			header( 'Content-type: application/json' );
			die( json_encode( array( 'data' => $result, 'text' => $new_postmeta ) ) );
		}

		/*
		 ****************************
		* manage columns functions *
		****************************
		*/

		public function dw_posts_table_column( $columns ) {
			return array_merge( $columns, array(
				'featured' => __( 'Featured' )
			) );
		}

		//render columns function
		public function dv_featured_column( $column_name, $id ) {

			if ( 'featured' !== $column_name ) {
				return;
			}

			$status = get_post_meta( $id, $this->metakey, true );

			if ( ! in_array( $status, array( 'yes', 'no' ) ) ) {
				$status = 'no';
			}

			$icon_class = ( $status === 'yes' ) ? 'dashicons-star-filled' : 'dashicons-star-empty';

			printf( '<div class="featured-status"><a href="#" class="featured-post" pid="%d"><span class="dashicons %s"></span></a></div>', $id, $icon_class, $status );

		}


		//js/jquery code to call ajax
		public function insert_ajax_status_script() {
			?>
            <script type="text/javascript">

                jQuery(document).ready(
                    function ($) {
                        $('.featured-post').click(
                            function (ev) {
                                ev.preventDefault();
                                link = $(this);
                                $.get(
                                    ajaxurl,
                                    {
                                        'action': 'change_post_featured_status',
                                        'post_id': $(this).attr('pid')
                                    },
                                    function (result) {
                                        if ('' !== result.text) {
                                            var icon = (result.text === 'yes') ? 'dashicons-star-filled' : 'dashicons-star-empty';
                                            link.html('<span class="dashicons ' + icon + '"></span>');
                                        }
                                        else {
                                            console.log(result.data)
                                        }
                                    }
                                );
                            }
                        );
                    }
                );

            </script>
			<?php
		}

	}
}


function dv_posts_query( $query ) {
	if ( ! is_admin() && $query->is_main_query() && is_home() ) {
		$query->set( 'posts_per_page', get_theme_mod( 'news_per_page' ) );
	}
}

add_action( 'pre_get_posts', 'dv_posts_query' );