<?php

class ThemeGemFrontEnd
{

    public function init()
    {
        add_action('wp_enqueue_scripts', array($this, 'including_styles_and_scripts'));
        add_action('init', array($this, 'register_menus'));
    }

    public function including_styles_and_scripts()
    {
        wp_enqueue_style('normalize', get_theme_file_uri() . '/stylesheets/normalize.min.css');
        wp_enqueue_style('semantic', get_theme_file_uri() . '/stylesheets/semantic.min.css');

        
        wp_enqueue_style('owl-base', get_theme_file_uri() . '/owl.carousel/owl.carousel.min.css');
        wp_enqueue_style('owl-theme', get_theme_file_uri() . '/owl.carousel/owl.theme.default.min.css');
        

        wp_enqueue_style('theme', get_theme_file_uri() . '/stylesheets/screen.css');


        wp_register_script('semantic', get_template_directory_uri() . '/javascripts/semantic.min.js', array('jquery'), '1.0.0', true);
        wp_enqueue_script('semantic');
        
        wp_register_script('owl', get_template_directory_uri() . '/owl.carousel/owl.carousel.min.js', array('jquery'), '1.0.0', true);
        wp_enqueue_script('owl');
        

        if (is_singular('deals')) {
            wp_register_script('clipboard', 'https://cdn.jsdelivr.net/clipboard.js/1.5.3/clipboard.min.js', array(), '1.0.0', true);
            wp_enqueue_script('clipboard');
        }

        wp_register_script('init', get_template_directory_uri() . '/javascripts/init.js', array('jquery'), '1.0.0', true);
        wp_enqueue_script('init');
    }

    public function register_menus()
    {

        register_nav_menus(array(
            'mobile-menu-top' => __('Mobile menu top', 'html5blank'),
            'mobile-menu-bottom' => __('Mobile menu bottom', 'html5blank'),
            'header-menu' => __('Header Menu', 'html5blank'), // Main Navigation
            'footer-menu' => __('Footer Menu', 'html5blank'), // Sidebar Navigation
            'sidebar-menu' => __('Sidebar Menu', 'html5blank') // Extra Navigation if needed (duplicate as many as you need!)
        ));

    }
    public static function menu($location, $menu_class = 'menu-wrap') {

        wp_nav_menu(
            array(
                'theme_location'  => $location,
                'menu'            => '',
                'container'       => 'div',
                'container_class' => '',
                'container_id'    => '',
                'menu_class'      => 'menu',
                'menu_id'         => '',
                'echo'            => true,
                'fallback_cb'     => 'wp_page_menu',
                'before'          => '',
                'after'           => '',
                'link_before'     => '',
                'link_after'      => '',
                'items_wrap'      => '<ul class="'.$menu_class.'">%3$s</ul>',
                'depth'           => 0,
                'walker'          => ''
            ));
    }

}