<?php

class dw_filter_widget extends WP_Widget
{
    function __construct()
    {
        parent::__construct(
            'dw_filter_widget',
            __('Search filter', 'html5blank'),
            array('description' => __('Search filter', 'html5blank'),)
        );
    }

    public function widget($args, $instance)
    {
        global $wp_query;

        $title = apply_filters('widget_title', $instance['title']);

        echo $args['before_widget'];
        ?>
        <?php if (is_search()): ?>
        <div class="search-res-block search-filter">

            <form method="get" id="filter" action="<?php echo home_url(); ?>" role="search">
                <h3><?php echo $instance['title']; ?></h3>
                <div class="ui icon input" style="display: none;">
                    <input type="text" name="s"
                           value="<?php echo (isset($_GET['s'])) ? $_GET['s'] : ''; ?>"
                           placeholder="<?php _e('Search Stores, Dealsb & Coupons.', 'html5blank'); ?>">

                    <input type="hidden" name="post_type" value="deals">
                </div>
                <div class="field">
                    <label for="free_shipping"><?php _e('Free shipping', 'html5blank'); ?></label>
                    <div class="ui checkbox">
                        <input type="checkbox" name="free_shipping"
                               id="free_shipping" <?php echo (isset($_GET['free_shipping'])) ? 'checked' : ''; ?>>
                        <label for="free_shipping"><?php _e('Free shipping', 'html5blank'); ?></label>
                    </div>
                </div>
                <div class="field">
                    <label for="featured"><?php _e('Featured', 'html5blank'); ?></label>
                    <div class="ui checkbox">
                        <input type="checkbox" name="featured"
                               id="featured" <?php echo (isset($_GET['featured'])) ? 'checked' : ''; ?>>
                        <label for="featured"><?php _e('Featured', 'html5blank'); ?></label>
                    </div>
                </div>
                <div class="field">
                    <label for="expired">Expired</label>
                    <div class="ui checkbox">
                        <input <?php echo (isset($_GET['expired'])) ? 'checked' : ''; ?> type="checkbox"
                                                                                         name="expired"
                                                                                         id="expired">
                        <label for="expired"><?php _e('Show expired', 'html5blank'); ?></label>
                    </div>
                </div>
                <button class="search-submit ui button" type="submit"
                        role="button"><?php _e('Search', 'html5blank'); ?></button>

            </form>
        </div>
    <?php else: ?>
        <div class="search-res-block search-filter">
            <h3><?php _e('This widget work only on search page', 'html5blank'); ?></h3>
        </div>
    <?php endif; ?>
        <?php
        echo $args['after_widget'];
    }

    public function form($instance)
    {
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = __('Title', 'html5blank');
        }
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>"
                   name="<?php echo $this->get_field_name('title'); ?>" type="text"
                   value="<?php echo esc_attr($title); ?>"/>
        </p>
        <?php
    }

    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
        return $instance;
    }
}

add_shortcode('widget', 'filter_widget_shortcode');

function filter_widget_shortcode()
{

    $atts = array(
        'title' => 'Filter',
    );

    $args = array(
        'before_widget' => '<div class="%s features-deals-block">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="title">',
        'after_title' => '</h3>');

    ob_start();

    the_widget('dw_filter_widget', $atts, $args);
    $output = ob_get_clean();

    return $output;
}