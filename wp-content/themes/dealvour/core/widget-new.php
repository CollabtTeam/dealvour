<?php


class dw_menu_latest_news_widget extends WP_Widget
{
    private $count = 4;

    function __construct()
    {
        parent::__construct(
            'dw_menu_news_widget',
            __('Menu latest news', 'html5blank'),
            array('description' => __('Menu latest news', 'html5blank'),)
            );
    }

    public function widget($args, $instance)
    {
        $title = apply_filters('widget_title', $instance['title']);

        echo $args['before_widget'];
        if (!empty($title))
            echo $args['before_title'] . $title . $args['after_title'];
        $query_args = array('post_type' => 'post', 'posts_per_page' => $this->count, 'order' => 'DESC');

        query_posts($query_args);
        ?>
        <a href="<?php echo get_permalink(get_option('page_for_posts')) ?>" class="back-buttons">
            <i class="reply icon"></i> <?php _e('View Articles', 'html5blank'); ?>
        </a>
        <div class="menu-article-wrapper">
            <?php
            if (have_posts()):while (have_posts()): the_post(); ?>

            <article class="news-item">
               <div class="menu-item-img">
                <?php if (has_post_thumbnail()): ?>
                    <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('news-widget'); ?></a>
                <?php endif; ?></div>
                <div class="title-block"><h3 class="title"><a href="<?php the_permalink(); ?>"> <?php dv_smart_truncate(get_the_title(), 55); ?></a></h3></div>
                <div class="sub-title-block"><p><?php dv_smart_truncate(get_the_excerpt(), 90); ?></p></div>
                <div class="continue-button-block"> <a class="continue-read" href="<?php the_permalink(); ?>"><?php _e('Continue Reading', 'html5blank'); ?></a>
                </div>
            </article>


            <?php
            endwhile;
            endif;
            echo '</div>';

            wp_reset_query();
            echo $args['after_widget'];
        }

        public function form($instance)
        {
            if (isset($instance['title'])) {
                $title = $instance['title'];
            } else {
                $title = __('Title', 'html5blank');
            }
            ?>
            <p>
                <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
                <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>"
                name="<?php echo $this->get_field_name('title'); ?>" type="text"
                value="<?php echo esc_attr($title); ?>"/>
            </p>
            <?php
        }

        public function update($new_instance, $old_instance)
        {
            $instance = array();
            $instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
            return $instance;
        }
} // Class wpb_widget ends here