<?php
require_once( 'dashboard.php' );

add_action( 'init', 'dv_add_deals_post_type' );
add_action( 'init', 'dv_init_dashboard_table' );
add_action( 'init', 'dv_deals_cat' );
add_action( 'pre_get_posts', 'dv_allow_search_for_deals_only' );

function the_freeshipping( $val ) {
	$result = false;
	switch ( $val ) {
		case '0':
			return $result;
			break;
		case '1':
			$result = '+ Free Shipping';
			break;
		case '2':
			$result = '+ Free* Shipping';
			break;
		default:
			$result = '+ ' . $val . ' Shipping';
			break;
	}
	echo $result;
}

function dv_allow_search_for_deals_only( $query ) {
	if ( ! is_admin() && is_search() && $query->is_main_query() ) {
		$query->set( 'post_type', 'deals' );
		$query->set( 'posts_per_page', get_theme_mod( 'search_per_page' ) ? get_theme_mod( 'search_per_page' ) : 10 );

		$meta = array();

		if ( isset( $_GET['expired'] ) ) {
			$meta[] = array(
				'relation' => 'OR',
				array(
					'key'     => 'offer_expiration_date',
					'value'   => date( 'm/d/Y h:i:s a', time() ),
					'compare' => '>=',
				),
				array(
					'key'     => 'offer_expiration_date',
					'value'   => date( 'm/d/Y h:i:s a', time() ),
					'compare' => '<',
				),
			);
		} else {
			$meta[] = array(
				'key'     => 'offer_expiration_date',
				'value'   => date( 'm/d/Y h:i:s a', time() ),
				'compare' => '<',
			);
		}

		if ( isset( $_GET['free_shipping'] ) ) {
			$meta[]           = array(
				'key'     => 'free_shipping',
				'value'   => 1,
				'compare' => '='
			);
			$meta['relation'] = 'AND';
		}
		if ( isset( $_GET['featured'] ) ) {
			$meta[]           = array(
				'key'     => 'featured',
				'value'   => 'yes',
				'compare' => '='
			);
			$meta['relation'] = 'AND';
		}

		$query->set( 'meta_query', $meta );

	}
}

function dv_deals_cat() {
	register_taxonomy_for_object_type( 'deals_cat', 'deals' );

	register_taxonomy(
		'deals_cat',
		'deals',
		array(
			'label'             => __( 'Deal categories' ),
			'show_ui'           => true,
			'show_tagcloud'     => false,
			'hierarchical'      => true,
			'show_admin_column' => true,
		)
	);
}

function dv_init_dashboard_table() {
	new DealsPostTable();
}

function dv_add_deals_post_type() {

	register_post_type( 'deals', // Register Custom Post Type
		array(
			'labels'       => array(
				'name'               => __( 'Deals', 'html5blank' ),
				'singular_name'      => __( 'Deal', 'html5blank' ),
				'add_new'            => __( 'Add New', 'html5blank' ),
				'add_new_item'       => __( 'Add new deal', 'html5blank' ),
				'edit'               => __( 'Edit', 'html5blank' ),
				'edit_item'          => __( 'Edit deal', 'html5blank' ),
				'new_item'           => __( 'New deal', 'html5blank' ),
				'view'               => __( 'View deal', 'html5blank' ),
				'view_item'          => __( 'View deal', 'html5blank' ),
				'search_items'       => __( 'Search deal', 'html5blank' ),
				'not_found'          => __( 'No deals found', 'html5blank' ),
				'not_found_in_trash' => __( 'No deals in trash', 'html5blank' )
			),
			'menu_icon'    => 'dashicons-tickets-alt',
			'public'       => true,
			'hierarchical' => true,
			'has_archive'  => true,
			'supports'     => array(
				'title',
				'editor',
				'excerpt',
				'thumbnail'
			),
			'can_export'   => true
		) );
}

if ( ! class_exists( 'DealsPostTable' ) ) {

	class DealsPostTable {

		public $metakey = 'featured';
		public $nonce = 'deals_post_class_nonce';

		//constarctor
		public function __construct() {

			global $pagenow, $typenow; //&& $typenow =='page'

			if ( is_admin() && $pagenow == 'edit.php' ) {
				add_filter( 'admin_footer', array( $this, 'insert_ajax_status_script' ) );
			}

			add_filter( 'manage_deals_posts_columns', array( $this, 'dw_posts_table_column' ) );
			add_action( 'manage_deals_posts_custom_column', array( $this, 'dv_featured_column' ), 10, 2 );

			//bulk action
			add_filter( 'bulk_actions-edit-deals', array( $this, 'dv_bulk_featured' ) );
			add_action( 'handle_bulk_actions-edit-deals', array( $this, 'dv_handle_bulk_featured' ), 10, 3 );

			add_filter( 'views_edit-deals', array( $this, 'dv_filter_featured' ) );
			add_action( 'pre_get_posts', array( $this, 'dv_filter_deals_featured' ) );

			//ajax function
			add_action( 'wp_ajax_change_featured_status', array( $this, 'ajax_change_featured_status' ) );
			add_filter( 'pre_get_posts', array( $this, 'dv_posts_per_page' ) );

			add_action( 'add_meta_boxes', array( $this, 'dv_add_post_meta_boxes' ), 10, 1 );
			add_action( 'save_post', array( $this, 'save_post_meta_boxes' ), 10, 2 );
		}

		public function dv_add_post_meta_boxes() {

			add_meta_box(
				'featured',
				esc_html__( 'Featured' ),
				array( $this, 'dv_render_metaboxes' ),
				'deals',
				'side',
				'high'
			);

		}

		public function dv_render_metaboxes( $post ) {
			wp_nonce_field( basename( __FILE__ ), $this->nonce );
			$featured = get_post_meta( $post->ID, $this->metakey, true ); ?>
            <p>
                <label for="dv-<?php echo $this->metakey; ?>">
                    <input type="checkbox" value="yes" name="<?php echo $this->metakey ?>"
                           id="dv-<?php echo $this->metakey; ?>" <?php if ( $featured === 'yes' ) {
						echo "checked ";
					} ?> />
					<?php _e( "Featured", 'example' ); ?>
                </label>
            </p>
		<?php }

		public function save_post_meta_boxes( $post_id, $post ) {
			if ( ! isset( $_POST[ $this->nonce ] ) || ! wp_verify_nonce( $_POST[ $this->nonce ], basename( __FILE__ ) ) ) {
				return $post_id;
			}


			$post_type = get_post_type_object( $post->post_type );

			if ( ! current_user_can( $post_type->cap->edit_post, $post_id ) ) {
				return $post_id;
			}

			$new_meta_value = ( isset( $_POST[ $this->metakey ] ) ? $_POST[ $this->metakey ] : '' );

			$meta_key = $this->metakey;

			$meta_value = get_post_meta( $post_id, $meta_key, true );

			if ( $new_meta_value && '' == $meta_value ) {
				add_post_meta( $post_id, $meta_key, $new_meta_value, true );
			} elseif ( $new_meta_value && $new_meta_value != $meta_value ) {
				update_post_meta( $post_id, $meta_key, $new_meta_value );
			} elseif ( '' == $new_meta_value && $meta_value ) {
				delete_post_meta( $post_id, $meta_key, $meta_value );
			}

		}

		public function change_post_status( $post_id, $status ) {
			$current_post                = get_post( $post_id, 'ARRAY_A' );
			$current_post['post_status'] = $status;
			wp_update_post( $current_post );
		}

		public function dv_handle_bulk_featured( $redirect_to, $doaction, $post_ids ) {
			if ( ( $doaction != 'featured' ) && ( $doaction != 'unfeatured' ) && ( $doaction != 'draft' ) && ( $doaction != 'publish' ) ) {
				return;
			}

			if ( ( $doaction == 'featured' ) || ( $doaction == 'unfeatured' ) ) {

				switch ( $doaction ) {
					case 'featured':
						$value      = 'yes';
						$meta_value = $this->metakey;
						break;
					case 'unfeatured':
						$value      = 'no';
						$meta_value = $this->metakey;
						break;
					default:
						break;
				}
				foreach ( $post_ids as $id ) {
					update_post_meta( $id, $meta_value, $value );
				}
			}


			if ( ( $doaction == 'draft' ) || ( $doaction == 'publish' ) ) {

				foreach ( $post_ids as $id ) {
					$this->change_post_status( $id, $doaction );
				}
			}

			return $redirect_to;
		}

		public function dv_bulk_featured( $bulk_actions ) {
			$bulk_actions['featured']   = __( 'Featured', 'html5blank' );
			$bulk_actions['unfeatured'] = __( 'Unfeatured', 'html5blank' );
			$bulk_actions['draft']      = __( 'Draft', 'html5blank' );
			$bulk_actions['publish']    = __( 'Publish', 'html5blank' );

			return $bulk_actions;
		}


		public function dv_filter_deals_featured( $query ) {
			if ( is_admin() && isset( $query->query['post_type'] ) && $query->query['post_type'] == 'deals' ) {

				if ( $query->get( 'orderby' ) == 'menu_order title' ) {
					$query->set( 'orderby', 'date' );
					$query->set( 'order', 'desc' );
				}

				if ( isset( $_GET['meta_key'] ) && ( $_GET['meta_key'] == 'featured' ) ) {
					$query->set( 'meta_key', 'featured' );
					$query->set( 'meta_value', 'yes' );
				}
			}

		}

		public function dv_filter_featured( $views ) {

			$class_name = ( isset( $_GET['meta_key'] ) && $_GET['meta_key'] == 'featured' ) ? "current" : "";

			$args = array(
				'post_type'  => 'deals',
				'meta_key'   => 'featured',
				'meta_value' => 'yes'
			);

			$result = new WP_Query( $args );

			$views['featured'] = sprintf(
				'<a  class="%s" href="%s">' . __( 'Featured' ) . ' <span class="count">(%d)</span></a>',
				$class_name,
				admin_url( 'edit.php?meta_key=featured&meta_value=yes&post_type=deals' ),
				$result->found_posts
			);


			return $views;

		}

		public function dv_posts_per_page( $query ) {
			if ( is_tax( 'deals_cat' ) ) {

				$query->query_vars['posts_per_page'] = get_theme_mod( 'deals_per_page' );

				return;
			}
		}

		public function ajax_change_featured_status() {

			$result       = '';
			$new_postmeta = '';
			$postid       = filter_input( INPUT_GET, 'post_id', FILTER_SANITIZE_NUMBER_INT );

			if ( ! isset( $postid ) ) {
				$result = 'something went wrong ...';
			}

			$old_postmeta = get_post_meta( $postid, $this->metakey, true );

			// if no status was saved, predefine the new status to no.
			if ( ! in_array( $old_postmeta, array( 'yes', 'no' ) ) ) {
				$old_postmeta = 'no';
			}

			$new_postmeta = ( $old_postmeta === 'yes' ) ?
				'no' : 'yes';

			if ( empty( $result ) ) {
				$result = 'change status to ' . $new_postmeta;
			}

			update_post_meta( $postid, $this->metakey, $new_postmeta );

			header( 'Content-type: application/json' );
			die( json_encode( array( 'data' => $result, 'text' => $new_postmeta, 'post_id' => $postid ) ) );
		}

		/*
		 ****************************
		* manage columns functions *
		****************************
		*/

		public function dw_posts_table_column( $columns ) {
			return array_merge( $columns, array(
				'featured' => __( 'Featured' )
			) );
		}

		//render columns function
		public function dv_featured_column( $column_name, $id ) {

			if ( 'featured' !== $column_name ) {
				return;
			}

			if ( 'date' === $column_name ) {
				echo 'herre';
			}

			$status = get_post_meta( $id, $this->metakey, true );

			if ( ! in_array( $status, array( 'yes', 'no' ) ) ) {
				$status = 'no';
			}

			$icon_class = ( $status === 'yes' ) ? 'dashicons-star-filled' : 'dashicons-star-empty';

			printf( '<div class="featured-status"><a href="#" class="featured-deal" pid="%d"><span class="dashicons %s"></span></a></div>', $id, $icon_class, $status );

		}


		//js/jquery code to call ajax
		public function insert_ajax_status_script() {
			?>
            <script type="text/javascript">

                jQuery(document).ready(
                    function ($) {
                        $('.featured-deal').click(
                            function () {
                                link = $(this);
                                $.get(
                                    ajaxurl,
                                    {
                                        'action': 'change_featured_status',
                                        'post_id': $(this).attr('pid')
                                    },
                                    function (result) {
                                        if ('' !== result.text) {
                                            var icon = (result.text === 'yes') ? 'dashicons-star-filled' : 'dashicons-star-empty';
                                            link.html('<span class="dashicons ' + icon + '"></span>');
                                        }
                                        else {
                                            console.log(result.data)
                                        }
                                    }
                                );
                            }
                        );
                    }
                );

            </script>
			<?php
		}

	}
}


//Featured Deals Un-check after exparing
add_action( 'wp_ajax_nopriv_clear_expired_featured', 'clear_expired_featured_cron' );
add_action( 'wp_ajax_clear_expired_featured', 'clear_expired_featured_cron' );
function clear_expired_featured_cron() {
	$query_args = array(
		'post_status'    => 'publish',
		'post_type'      => 'deals',
		'posts_per_page' => - 1,
		'meta_key'       => 'featured',
		'meta_value'     => 'yes'
	);
	$posts      = query_posts( $query_args );

	$flag = false;
	foreach ( $posts as $post ) {
		$is_expired = is_expired( get_field( 'offer_expiration_date', $post->ID ) );
		if ( $is_expired ) {
			$flag = true;
			echo $post->ID . ' ' . $post->post_title . "\r\n";
			delete_post_meta( $post->ID, 'featured' );
		}
	}
	if ( $flag ) {
		echo 'Unfeatured expired deals complete';
	} else {
		echo 'Nothing to flush';
	}
	wp_die();
}


function admin_bar_unfeatured( $wp_admin_bar ) {
	$args = array(
		'id'    => 'unfeatured-expired',
		'title' => 'Flush expired deals',
		'href'  => site_url() . '/wp-admin/admin-ajax.php?action=clear_expired_featured',
		'meta'  => array(
			'class' => 'flush-expired'
		)
	);
	$wp_admin_bar->add_node( $args );
}

add_action( 'admin_bar_menu', 'admin_bar_unfeatured', 50 );


function top_bar_button_script() {
	wp_register_script( 'admin-js', get_template_directory_uri() . '/javascripts/admin.js' );
	wp_localize_script( 'admin-js', 'dealvour', array(
			'ajax_url' => admin_url( "admin-ajax.php" )
		)
	);
	wp_enqueue_script( 'admin-js' );

}

add_action( 'admin_enqueue_scripts', 'top_bar_button_script' );
add_action( 'wp_enqueue_scripts', 'top_bar_button_script' );

add_filter( 'post_date_column_time', 'extend_table_with_time', 10, 4 );

function extend_table_with_time( $h_time, $post ) {
	echo '<abbr>' . $post->post_date . '</abbr>';
}