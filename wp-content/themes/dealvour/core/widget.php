<?php


class dw_deals_widget extends WP_Widget
{
    private $order = array(
        array('value' => 'latest', 'text' => 'Latest deals'),
        array('value' => 'featured', 'text' => 'Featured deals')
    );
    private $count = 5;

    function __construct()
    {
        parent::__construct(
            'dw_deals_widget',
            __('Deals widget', 'html5blank'),
            array('description' => __('Deals widget', 'html5blank'),)
        );
    }

    public function widget($args, $instance)
    {
        $title = apply_filters('widget_title', $instance['title']);

        echo $args['before_widget'];
        if (!empty($title))
            echo $args['before_title'] . $title . $args['after_title'];
        $query_args = array('post_type' => 'deals', 'posts_per_page' => $instance['count'], 'order' => 'DESC');

        if ($instance['order'] == 'featured') {
            $query_args = array_merge($query_args,
                array(
                    'meta_key' => 'featured',
                    'meta_value' => 'yes',
                    'orderby' => get_theme_mod('random_order_deals') ? 'rand' : 'DESC'
                )
            );
        }
        query_posts($query_args);

        $random_class = get_theme_mod('random_order_deals') ? 'owl-random' : '';

        if (have_posts()): ?>
            <div class="<?php echo $random_class; ?>">
                <?php
                while (have_posts()): the_post(); ?>
                    <div class="features-details">
                        <div class="widget-features-details-img item">
                            <?php if (has_post_thumbnail()): ?>
                                <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail('small'); ?></a>
                            <?php endif; ?>

                        </div>
                        <p><a href="<?php the_permalink(); ?>"><?php the_title(); ?>
                                <?php if (get_field('sub_title')): ?>
                                    <span><?php the_field('sub_title') ?></span>
                                <?php endif; ?>
                                <span class="price">
                    <?php if (get_field('sale_price')) {
                        the_field('sale_price');
                    } ?>

                    <?php if (get_field('free_shipping')): ?>
                        <?php the_freeshipping(get_field('free_shipping')) ?>
                    <?php endif; ?>
                </span>
                            </a>
                        </p>
                    </div>

                    <?php
                endwhile; ?>
            </div>
        <?php endif;
        wp_reset_query();
        echo $args['after_widget'];
    }

    public function form($instance)
    {
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = __('Title', 'html5blank');
        }

        if (isset($instance['order'])) {
            $selected = $instance['order'];
        } else {
            $selected = $this->order[0]['value'];
        }

        if (isset($instance['count']) && ($instance['count'] > 0)) {
            $count = $instance['count'];
        } else {
            $count = $this->count;
        }
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>"
                   name="<?php echo $this->get_field_name('title'); ?>" type="text"
                   value="<?php echo esc_attr($title); ?>"/>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('count'); ?>"><?php _e('Show deals:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('count'); ?>"
                   name="<?php echo $this->get_field_name('count'); ?>" type="text"
                   value="<?php echo esc_attr($count); ?>"/>
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('order'); ?>"><?php _e('Show deals:'); ?></label>
            <select class="widefat" name="<?php echo $this->get_field_name('order'); ?>"
                    id="<?php echo $this->get_field_id('order'); ?>">
                <?php foreach ($this->order as $item) : ?>
                    <option <?php if ($item['value'] == $selected) {
                        echo 'selected';
                    } ?>
                            value="<?php echo $item['value']; ?>"><?php echo $item['text']; ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </p>
        <?php
    }

    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
        $instance['count'] = (!empty($new_instance['count'])) ? $new_instance['count'] : '';
        $instance['order'] = (!empty($new_instance['order'])) ? $new_instance['order'] : '';
        return $instance;
    }
} // Class wpb_widget ends here