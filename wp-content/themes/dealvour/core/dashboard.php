<?php


if (function_exists('add_theme_support')) {
    add_theme_support('menus');
}


require_once('customizer.php');
require_once('widget.php');
require_once('widget-menu.php');
require_once('widget-new.php');
require_once('widget-filter.php');

add_action('widgets_init', 'dw_load_widgets');

function dw_load_widgets()
{
    register_widget('dw_deals_widget');
    register_widget('dw_filter_widget');
    register_widget('dw_category_deals_widget');
    register_widget('dw_menu_latest_news_widget');
}




if (function_exists('register_sidebar')) {
    register_sidebar(array(
        'name' => __('News & Article Blog - Right Side Adverts & Deal Section Placements', 'html5blank'),
        'description' => __('News & Article Blog - Right Side Adverts & Deal Section Placements', 'html5blank'),
        'id' => 'sidebar_widgets',
        'before_widget' => '<div id="%1$s" class="%2$s features-deals-block">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="title">',
        'after_title' => '</h3>'
    ));

    register_sidebar(array(
        'name' => __('Site Header Advert Placement', 'html5blank'),
        'description' => __('Site Header Advert Placement', 'html5blank'),
        'id' => 'after_header',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '',
        'after_title' => ''
    ));

    register_sidebar(array(
        'name' => __('Site Footer Advert Placement', 'html5blank'),
        'description' => __('Site Footer Advert Placement', 'html5blank'),
        'id' => 'before_footer',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '',
        'after_title' => ''
    ));

    register_sidebar(array(
        'name' => __('Top of Search Advert Placement', 'html5blank'),
        'description' => __('Top of Search Advert Placement', 'html5blank'),
        'id' => 'search_top',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '',
        'after_title' => ''
    ));

    register_sidebar(array(
        'name' => __('News & Article Blog Middle Advert Placement', 'html5blank'),
        'description' => __('News & Article Blog Middle Advert Placement', 'html5blank'),
        'id' => 'news_middle',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '',
        'after_title' => ''
    ));

    register_sidebar(array(
        'name' => __('Deal FrontPage Trending Articles Right Advert Placement', 'html5blank'),
        'description' => __('Deal FrontPage Trending Articles Right Advert Placement', 'html5blank'),
        'id' => 'trand_article',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '',
        'after_title' => ''
    ));

    register_sidebar(array(
        'name' => __('Today’s Deals FrontPage Middle Advert Placement', 'html5blank'),
        'description' => __('Today’s Deals FrontPage Middle Advert Placement', 'html5blank'),
        'id' => 'today_middle_advert',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '',
        'after_title' => ''
    ));

    register_sidebar(array(
        'name' => __('Deals Category Middle Section Advert Placement', 'html5blank'),
        'description' => __('Deals Category Middle Section Advert Placement', 'html5blank'),
        'id' => 'deals_middle_article',
        'before_widget' => '<div id="%1$s" class="%2$s">',
        'after_widget' => '</div>',
        'before_title' => '',
        'after_title' => ''
    ));

    register_sidebar(array(
        'name' => __('Deal Information Page Bottom Advert Placement', 'html5blank'),
        'description' => __('Deal Information Page Bottom Advert Placement', 'html5blank'),
        'id' => 'sidebar_widgets_deal_bottom',
        'before_widget' => '<div id="%1$s" class="%2$s features-deals-block">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="title">',
        'after_title' => '</h3>'
    ));

    register_sidebar(array(
        'name' => __('Search page sidebar', 'html5blank'),
        'description' => __('Search page sidebar', 'html5blank'),
        'id' => 'sidebar_search',
        'before_widget' => '<div id="%1$s" class="%2$s features-deals-block">',
        'after_widget' => '</div>',
        'before_title' => '<h3 class="title">',
        'after_title' => '</h3>'
    ));


}