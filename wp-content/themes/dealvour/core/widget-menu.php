<?php

class dw_category_deals_widget extends WP_Widget
{
    function __construct()
    {
        parent::__construct(
            'dw_deals_category_widget',
            __('Deals category widget', 'html5blank'),
            array('description' => __('Deals category widget', 'html5blank'),)
        );
    }

    public function widget($args, $instance)
    {
        global $wp_query;

        $title = apply_filters('widget_title', $instance['title']);

        echo $args['before_widget'];
        if (!empty($title))
            echo $args['before_title'] . $title . $args['after_title'];

        $terms = get_terms(array(
            'taxonomy' => 'deals_cat',
        ));
        $active = null;

        if (isset($wp_query->query_vars['deals_cat'])) {
            $active = $wp_query->query_vars['deals_cat'];
        }
        if (is_singular('deals')) {
            global $post;
            $my_terms = get_the_terms( $post->ID, 'deals_cat' );
            if( $my_terms && !is_wp_error( $my_terms ) ) {
                foreach( $my_terms as $term ) {
                    $active = $term->slug;
                }
            }
        }
        if (count($terms) > 0) {
            echo '<ul>';
        }

        foreach ($terms as $term) {
            $active_class = ($active === $term->slug) ? "active" : " ";
            echo "<li class='" . $active_class . "'><a  href='" . get_term_link($term) . "'>" . $term->name . "</a></li>";
        }

        if (count($terms) > 0) {
            echo '</ul>';
        }
        wp_reset_query();
        echo $args['after_widget'];
    }

    public function form($instance)
    {
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = __('Title', 'html5blank');
        }
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>"
                   name="<?php echo $this->get_field_name('title'); ?>" type="text"
                   value="<?php echo esc_attr($title); ?>"/>
        </p>
        <?php
    }

    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
        return $instance;
    }
}