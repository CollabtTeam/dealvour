<?php

add_action('after_setup_theme', 'dv_theme_setup');
add_action('customize_register', 'dv_exted_customizer');

function dv_theme_setup()
{
    add_theme_support('custom-logo');
}


function dv_exted_customizer($wp_customize)
{
    //Footer section
    $wp_customize->add_section('footer_section', array(
        'title' => 'Footer'
    ));

    $wp_customize->add_setting('footer_copyright', array(
        'default' => 'Default Text For copyright Section',
    ));

    $wp_customize->add_control('footer_copyright', array(
        'label' => 'Copyright text',
        'section' => 'footer_section',
        'type' => 'text',
    ));

    $wp_customize->add_setting('facebook_link', array(
        'default' => '',
    ));

    $wp_customize->add_control('facebook_link', array(
        'label' => 'Facebook link',
        'section' => 'footer_section',
        'type' => 'text',
    ));


    $wp_customize->add_setting('instagram_link', array(
        'default' => '',
    ));

    $wp_customize->add_control('instagram_link', array(
        'label' => 'Instagram link',
        'section' => 'footer_section',
        'type' => 'text',
    ));

    $wp_customize->add_setting('pinterest_link', array(
        'default' => '',
    ));

    $wp_customize->add_control('pinterest_link', array(
        'label' => 'Instagram link',
        'section' => 'footer_section',
        'type' => 'text',
    ));

    $wp_customize->add_setting('twitter_link', array(
        'default' => '',
    ));

    $wp_customize->add_control('twitter_link', array(
        'label' => 'Twitter link',
        'section' => 'footer_section',
        'type' => 'text',
    ));

    //Static front page
    $wp_customize->add_setting('show_featured_slider', array(
        'default' => true,
    ));

    $wp_customize->add_control('show_featured_slider', array(
        'label' => 'Show/Hide Featured deals section ',
        'section' => 'static_front_page',
        'type' => 'checkbox',
    ));

    $wp_customize->add_setting('enable_featured_slider', array(
        'default' => true,
    ));

    $wp_customize->add_control('enable_featured_slider', array(
        'label' => 'On/Off Featured deals slider(hide arrows)',
        'section' => 'static_front_page',
        'type' => 'checkbox',
    ));

    //Deals options
    $wp_customize->add_section('deals_section', array(
        'title' => 'Deals options'
    ));

    $wp_customize->add_setting('random_order_deals', array(
        'default' => true,
    ));

    $wp_customize->add_control('random_order_deals', array(
        'label' => 'Randomize featured deals',
        'section' => 'deals_section',
        'type' => 'checkbox',
    ));

    $wp_customize->add_setting('today_deals_per_page', array(
        'default' => '40',
    ));

    $wp_customize->add_control('today_deals_per_page', array(
        'label' => 'Today’s Hot Deals FrontPage – Total Deals Per Page Count:',
        'section' => 'deals_section',
        'type' => 'number',
    ));

    $wp_customize->add_setting('show_advert_hot_deal_after', array(
        'default' => '20',
    ));

    $wp_customize->add_control('show_advert_hot_deal_after', array(
        'label' => 'Today’s Hot Deals FrontPage - Advert Displays Per Deal Count: ',
        'section' => 'deals_section',
        'type' => 'number',
    ));

    $wp_customize->add_setting('top_deal_advert_repeat', array(
        'default' => false,
    ));

    $wp_customize->add_control('top_deal_advert_repeat', array(
        'label' => 'Top deals - turn on Advert Repeat',
        'section' => 'deals_section',
        'type' => 'checkbox',
    ));

    $wp_customize->add_setting('deals_per_page', array(
        'default' => '60',
    ));

    $wp_customize->add_control('deals_per_page', array(
        'label' => 'Deal Category Pages – Total Deals Per Page Count:',
        'section' => 'deals_section',
        'type' => 'number',
    ));

    $wp_customize->add_setting('show_advert_deal_after', array(
        'default' => '30',
    ));

    $wp_customize->add_control('show_advert_deal_after', array(
        'label' => 'Deal Category -  Advert Displays Per Deal Count: ',
        'section' => 'deals_section',
        'type' => 'number',
    ));

    $wp_customize->add_setting('deal_advert_repeat', array(
        'default' => false,
    ));

    $wp_customize->add_control('deal_advert_repeat', array(
        'label' => 'Turn on Advert Repeat',
        'section' => 'deals_section',
        'type' => 'checkbox',
    ));

    //News options
    $wp_customize->add_section('news_section', array(
        'title' => 'News options'
    ));

    $wp_customize->add_setting('show_featured_news', array(
        'default' => true,
    ));

    $wp_customize->add_control('show_featured_news', array(
        'label' => 'Show/Hide Featured news section',
        'section' => 'news_section',
        'type' => 'checkbox',
    ));

    //Blog section
    $wp_customize->add_section('blog_section', array(
        'title' => 'Blog options'
    ));

    $wp_customize->add_setting('news_per_page', array(
        'default' => '10',
    ));

    $wp_customize->add_control('news_per_page', array(
        'label' => 'Blog Article FrontPage  – Total Article Post Per Page:',
        'section' => 'blog_section',
        'type' => 'number',
    ));

    $wp_customize->add_setting('news_advert_after', array(
        'default' => '5',
    ));

    $wp_customize->add_control('news_advert_after', array(
        'label' => 'Blog Article Frontpage - Advert Displays Per Article Count: ',
        'section' => 'blog_section',
        'type' => 'number',
    ));

    $wp_customize->add_setting('news_advert_repeat', array(
        'default' => false,
    ));

    $wp_customize->add_control('news_advert_repeat', array(
        'label' => 'Turn on Advert Repeat',
        'section' => 'blog_section',
        'type' => 'checkbox',
    ));

    //Search section
    $wp_customize->add_section('search_section', array(
        'title' => 'Search page'
    ));

    $wp_customize->add_setting('search_per_page', array(
        'default' => '10',
    ));

    $wp_customize->add_control('search_per_page', array(
        'label' => 'Search page post result:',
        'section' => 'search_section',
        'type' => 'number',
    ));

}