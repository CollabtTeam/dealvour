<?php if (have_posts()): $index = 1;
while (have_posts()) : the_post(); ?>
<article id="post-<?php the_ID(); ?>" class="post-block <?php the_blog_class($index); ?>">
    <?php if (has_post_thumbnail()) : ?>
        <a class="post-image post-image-desktop" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
            <?php the_post_thumbnail('blog-thumbnails'); ?>
        </a>

        <a class="post-image post-image-mobile" href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
            <?php the_post_thumbnail('trending-thumbnails-mobile') ?>
        </a>


    <?php endif; ?>
    <div class="post-content">
      <div class="post-details">
        <a href="<?php the_permalink(); ?>">
            <h3 class="post-title"><?php the_title(); ?></h3>
        </a>
    </div>
    <div class="post-buttons">
        <?php the_category() ?>
    </div>
    <div class="post-details">
        <!-- <span class="post-date"><?php the_time('F j, Y'); ?> by <?php the_author_posts_link(); ?></span> -->
        <a href="<?php the_permalink(); ?>">
            <div class="post-description"><?php dv_smart_truncate(get_the_excerpt(), 250); ?></div>
        </a>
    </div>
    
    <div class="view-button-wrapper">
        <a href="<?php the_permalink(); ?>" class="view-button"><?php _e('Continue reading', 'html5blank'); ?></a>
    </div>

</div>
</article>
<?php do_action('after_post_content', $index, !empty(get_previous_post())); ?>
<?php $index++; ?>
<?php endwhile; ?>

<?php else: ?>

    <!-- article -->
    <article>
        <h2><?php _e('Sorry, nothing to display.', 'html5blank'); ?></h2>
    </article>
    <!-- /article -->

<?php endif; ?>
