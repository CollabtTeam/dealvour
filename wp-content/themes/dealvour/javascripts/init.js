(function ($) {

    $.fn.shuffle = function () {

        var allElems = this.get(),
            getRandom = function (max) {
                return Math.floor(Math.random() * max);
            },
            shuffled = $.map(allElems, function () {
                var random = getRandom(allElems.length),
                    randEl = $(allElems[random]).clone(true)[0];
                allElems.splice(random, 1);
                return randEl;
            });

        this.each(function (i) {
            $(this).replaceWith($(shuffled[i]));
        });

        return $(shuffled);

    };

    $(window).load(function () {
        if ($('body').hasClass('single-deals')) {
            var widgetHeight = $('.right-sidebar').height();
            $('single-deals').find('.article-news').css('min-height', widgetHeight + 'px');
        }
    });

    $(document).ready(function () {
        $('#es_txt_email_pg').attr('placeholder', 'Enter Email Address');
        attachPopup();

        $('.JS-featured-carousel').owlCarousel({
            loop: true,
            autoplay: true,
            autoplayTimeout: 3000,
            items: 1,
            dots: true,
            center: true
        });
    });

    function attachPopup() {
        $('.card-buttons .share-popup-button').each(function () {
            $(this).popup({
                popup: $(this).parent('.card-buttons').next('.popup'),
                on: 'hover',
                exclusive: true,
                hoverable: true,
                delay: {
                    show: 100,
                    hide: 600
                },
                position: 'top left'
            });
        });
    }

    $('#es_txt_button_pg').click(function () {
        $('#es_msg_pg').text('');
    });

    if ($('.JS-homePage-carousel > *').length > 4) {
        var owl = $('.JS-homePage-carousel').shuffle();

        owl.owlCarousel({
            loop: true,
            margin: 0,
            nav: true,
            navText: ["<i class='arrow left icon'></i>", "<i class='arrow right icon'></i>"],
            responsiveClass:true,
            responsive:{
                0: {
                    items: 1
                },
                480: {
                    items: 2
                },
                992:{
                    items: 4
                }
            },
            dots: false,
            onLoaded: function () {
                attachPopup();
            }
        })
    }


    if (!!currentPage) {
        switch (currentPage) {
            case 'deal':
                $('a.mega-menu-link[href="http://dealvour.com/"], .mobile-menu a.mega-menu-link[href="http://dealvour.com/"]').parent().addClass('mega-current-menu-item');
                break;
            case 'news':
                $('a.mega-menu-link[href*="/blog/"], .mobile-menu a.mega-menu-link[href*="/blog/"]').parent().addClass('mega-current-menu-item');
                break;
            default:
                break;

        }
    }

    if ((typeof Clipboard != 'undefined') && $('.js-copy').size() > 0) {
        var clipboard = new Clipboard('.js-copy');

        clipboard.on('success', function (e) {
            var text = $('.js-copy').html();
            $('.js-copy').text('Code Copied!');
            setTimeout(function () {
                $('.js-copy').html(text)
            }, 5000);
        });
    }

    $('.flush-expired a').on('click', function (ev) {
        ev.preventDefault();
        var data = {
            action: 'clear_expired_featured',
        };

        $.get(ajaxurl, data, function (response) {
            alert(response);
        });
    });

    $(".open-btn").click(function () {
        $('body').css('overflow', 'hidden');
        $('.mobile-menu-wrapper').addClass('open');
        $('.mobile-overlay').addClass('active');
        $(".mobile-menu").addClass('open');
        $(this).removeClass('active');
        $('.close-btn').addClass('active');
    });

    $(".close-btn").click(function () {
        $('body').css('overflow', 'auto');
        $('.mobile-menu-wrapper').removeClass('open');
        $('.mobile-overlay').removeClass('active');
        $(".mobile-menu").removeClass('open');
        $(this).removeClass('active');
        $('.open-btn').addClass('active');
    });


    var featuredslider = $('.JS-featured-carousel-desctop').owlCarousel({
        items: 1,
        loop: true,
        center: false,
        margin: 0,
        URLhashListener: true,
        autoplayHoverPause: true,
        dots: false,
        nav: false,
        autoplay: true,
        autoplayTimeout: 3000,
        onInitialize: function () {
            $('.fearured-carousel-nav .featured-nav:first').addClass('active');
        }

    });

    featuredslider.on('changed.owl.carousel', function (e) {
        higlightActive();
    });


    function higlightActive() {
        var href = location.hash;
        $('.fearured-carousel-nav .featured-nav').removeClass('active');
        $('.fearured-carousel-nav .featured-nav[href=' + href + ']').addClass('active');
    }


    $('.fearured-carousel-nav .featured-nav').mouseover(function () {
        if (!$(this).hasClass('active')) {
            location.href = location.origin + location.pathname + $(this).attr('href');
        }
    });

})(jQuery);
