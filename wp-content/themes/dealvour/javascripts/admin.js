jQuery(document).ready(function ($) {

    $('.flush-expired a').on('click', function (ev) {
        ev.preventDefault();
        var data = {
            action: 'clear_expired_featured',
        };
        $(this).css({
            'pointer-events': 'none',
            'cursor': 'default',
            'color': '#666'
        });
        $.get(dealvour.ajax_url, data, function (response) {
            alert(response);
            $('.flush-expired a').removeAttr('style');
        });
    });

});