<?php if (is_active_sidebar('deals_middle_article')) : ?>
    <div class="advertising-banner">
        <div class="ui container">
            <?php dynamic_sidebar('deals_middle_article'); ?>
        </div>
    </div>
<?php endif; ?>
