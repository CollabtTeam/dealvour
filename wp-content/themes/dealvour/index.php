<?php get_header(); ?>

<main role="main">
    <div class="product-content-wrapper">
        <?php get_template_part('partials/advert-after-header') ?>
        <div class="product-news-content">
            <div class="ui grid container">
                <div class="sixteen wide tablet eleven wide computer column">
                    <?php get_template_part('partials/featured-post'); ?>
                    <?php include('loop.php'); ?>
                    <?php get_template_part('short-pagination'); ?>
                </div>
                <?php get_sidebar(); ?>
            </div>
        </div>
        <?php get_template_part('partials/advert-before-footer') ?>
    </div>

</main>

<?php get_footer(); ?>
