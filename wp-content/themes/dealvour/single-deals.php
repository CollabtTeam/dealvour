<?php get_header(); ?>
<main role="main">
    <div class="product-content-wrapper">
        <?php get_template_part('partials/advert-after-header') ?>
        <div class="product-news-content">
            <div class="ui grid container">
                <div class="sixteen wide tablet eleven wide computer column">
                    <section class="article-news" id="post-<?php the_ID(); ?>">
                        <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                            <a href="javascript:void(0);"><h2 class="title"><?php the_title(); ?></h2></a>
                            <span class="date-news"><?php _e('Posted: ', 'html5blank'); ?><?php the_time('F j, Y'); ?></span>


                            <div class="news-content">
                                <?php if (has_post_thumbnail()) : ?>
                                    <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                        <?php the_post_thumbnail('large'); ?>
                                    </a>
                                <?php endif; ?>
                                <?php get_template_part('partials/sidebar-deals'); ?>
                                <?php the_content(); ?>
                            </div>
                        <?php endwhile; endif;
                        wp_reset_query(); ?>

                    </section>
                </div>
                <?php get_sidebar(); ?>
            </div>

            <div class="ui grid container">
                <?php
                $query_args = array(
                    'post_status' => 'publish',
                    'post_type' => 'deals',
                    'posts_per_page' => 4,
                    'orderby' => get_theme_mod('random_order_deals') ? 'rand' : 'DESC',
                    'meta_key' => 'featured',
                    'meta_value' => 'yes'
                );
                $random_class = get_theme_mod('random_order_deals') ? 'owl-random' : '';
                query_posts($query_args);
                if (have_posts()): ?>

                <div class="products-section">
                    <div class="ui container">
                        <div class="products-wrapper">
                            <div class="title-wrapper">
                                <h2 class="title"><?php _e('Featured deals', 'html5blank') ?></h2>
                            </div>
                            <div class="products-card-wrapper <?php echo $random_class; ?>">
                                <?php
                                while (have_posts()): the_post();
                                    include "loop-deal.php";
                                endwhile;
                                wp_reset_postdata();
                                ?>
                            </div>

                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>

        <div class="ui grid container">
            <?php
            $query_args = array(
                'post_status' => 'publish',
                'post_type' => 'deals',
                'posts_per_page' => 4,
                'order' => 'DESC'
            );

            query_posts($query_args);
            if (have_posts()): ?>

            <div class="products-section">
                <div class="ui container">
                    <div class="products-wrapper">
                        <div class="title-wrapper">
                            <h2 class="title"><?php _e('Latest Deals & Coupons', 'html5blank') ?></h2>
                        </div>
                        <div class="products-card-wrapper">
                            <?php
                            while (have_posts()): the_post();
                                include "loop-deal.php";
                            endwhile;
                            wp_reset_postdata();
                            ?>
                        </div>

                    </div>
                </div>
            </div>
        <?php endif; ?>
    </div>

</div>
<?php get_template_part('partials/advert-before-footer') ?>
</div>

</main>

<?php get_footer(); ?>
