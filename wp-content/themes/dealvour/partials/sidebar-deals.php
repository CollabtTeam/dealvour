
<div class="section">
    <div class="info-price-box">
        <div class="price-wrapper">
            <?php if (get_field('sale_price')): ?>
                <p class="price">
                    <?php the_field('sale_price') ?>
                </p>
            <?php endif; ?>
        </div>
        <div class="old-price-wrapper">
            <p class="old-price">
                <?php if (get_field('regular_price')): ?>
                    <?php _e('List price ', 'html5blank'); ?>
                    <span><?php the_field('regular_price') ?></span>
                <?php endif; ?>
            </p>
        </div>
        <div class="free-shipping-wrapper">
            <p class="free-shipping">
                <?php if (get_field('free_shipping')): ?>
                    <?php the_freeshipping(get_field('free_shipping'))?>
                <?php endif; ?>
            </p>
        </div>
        <div class="price-button-wrapper">
            <a class="price-button" target="_blank" href="<?php acf_the_url(get_field('offer_url')); ?>">
                <?php if (is_expired(get_field('offer_expiration_date'))) {
                    _e('Expired', 'html5blank');
                } else if (get_field('coupon_code')) {
                    _e('Use code', 'html5blank');
                } else {
                    _e('View deal', 'html5blank');
                }
                ?>
            </a>
        </div>
        <div class="social-buttons">
            <p class="title"><?php _e('Share this deal:', 'html5blank'); ?></p>
            <?php do_action('render_share_icon'); ?>
        </div>
        <?php if (get_field('coupon_code')) : ?>
            <div class="discount-block">
                <p class="discount js-copy" data-clipboard-text="<?php the_field('coupon_code'); ?>" title="Click to copy code to clipboard">
                    <?php _e('COPY CODE: ', 'html5blank'); ?>
                <span id="span"><?php the_field('coupon_code'); ?></span></p>
            </div>
        <?php endif; ?>
    </div>
</div>