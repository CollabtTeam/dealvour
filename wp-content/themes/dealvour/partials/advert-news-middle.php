<?php if ( is_active_sidebar( 'news_middle' ) ) : ?>
    <div class="advertising-banner">
        <div class="ui container">
            <?php dynamic_sidebar( 'news_middle' ); ?>
        </div>
    </div>
<?php endif; ?>
