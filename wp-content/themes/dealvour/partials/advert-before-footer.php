<?php if ( is_active_sidebar( 'before_footer' ) ) : ?>
	<div class="advertising-banner">
		<div class="ui container">
			<div class="advert-wrapper"> <?php dynamic_sidebar( 'before_footer' ); ?></div>
		</div>
	</div>
<?php endif; ?>
