<!--  Deals middle section -->
<?php if (is_active_sidebar('deals_middle_article')) : ?>
    <div class="advertising-banner-inline">
        <div class="ui container">
            <?php dynamic_sidebar('deals_middle_article'); ?>
        </div>
    </div>
<?php endif; ?>
<!-- Deals middle section end -->
