<?php global $frontend; ?>
<div class="mobile-overlay"></div>
<div class="mobile-button-wrapper">
	<a class="open-btn active" href="javascript:void(0);"></a>
</div>
<div class="mobile-menu-wrapper">
	<a class="close-btn" href="javascript:void(0);"></a>
	<div class="mobile-menu">
		<?php $frontend::menu('mobile-menu-top', 'mobile-menu-top-wrapper') ?>
		<?php $frontend::menu('mobile-menu-bottom', 'mobile-menu-bottom-wrapper') ?>
	</div>
</div>
