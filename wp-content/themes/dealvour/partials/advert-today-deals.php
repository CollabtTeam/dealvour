<!-- Today Hot deals middle section -->
<?php if (is_active_sidebar('today_middle_advert')) : ?>
    <div class="advertising-banner-inline">
        <div class="ui container">
            <?php dynamic_sidebar('today_middle_advert'); ?>
        </div>
    </div>
<?php endif; ?>
<!-- Today Hot deals middle section end -->
