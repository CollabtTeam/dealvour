<?php
if ( get_theme_mod( 'show_featured_news' ) ):
	$query_args = array(
		'post_status'    => 'publish',
		'post_type'      => 'post',
		'posts_per_page' => 5,
		'orderby'        => 'rand',
		'meta_key'       => 'featured',
		'meta_value'     => 'yes'
	);

	query_posts( $query_args ); ?>
	<?php if ( have_posts() ): ?>
    <div class="products-wrapper">
        <div class="featured-wrapper desctop-only">
            <div class="featured-caruosel-column">
                <div class="featured-thumb-block">
                    <div class="text"><?php _e( 'Featured', 'html5blank' ) ?></div>
                    <div class="owl-carousel owl-random owl-theme JS-featured-carousel-desctop">
						<?php while ( have_posts() ): the_post(); ?>

                            <div class="featured-thumb-carousel"
                                 style="background-image: url('<?php echo get_the_post_thumbnail_url( get_the_ID(), 'featured-thumb' ); ?>');"
                                 data-hash="id<?php echo get_the_ID() ?>">
                            </div>

						<?php endwhile; ?>
                    </div>
                </div>
            </div>

            <div class="featured-caruosel-column">
                <div class="featured-title-wrapper">
                    <ul class="fearured-carousel-nav owl-random">
						<?php while ( have_posts() ): the_post(); ?>
                            <li class="featured-nav" href="#id<?php echo get_the_ID() ?>">
                                <a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
                            </li>
						<?php endwhile; ?>
                    </ul>
                </div>
            </div>
			<?php wp_reset_query(); ?>

        </div>


        <div class="mobile-featured-wrapper mobile-only">
            <div class="owl-carousel owl-theme JS-featured-carousel">
				<?php while ( have_posts() ): the_post(); ?>
                    <div class="item">
                        <div class="mobile-featured-thumb-wrapper">
                            <div class="text"><?php _e( 'Featured', 'html5blank' ) ?> </div>
                            <div class="featured-thumb"
                                 style="background-image: url('<?php echo get_the_post_thumbnail_url( get_the_ID(), 'featured-thumb' ); ?>')"
                                 data-id="<?php echo get_the_ID() ?>">
                            </div>
                            <div class="mobile-featured-title-wrapper">
                                <ul>
                                    <li class="featured-nav" data-id="<?php echo get_the_ID() ?>">
                                        <a href="<?php the_permalink() ?>"><?php the_title(); ?></a>
                                    </li>
                                </ul>
                            </div>
                        </div>

                    </div>
				<?php endwhile; ?>
            </div>


        </div>
    </div>
<?php endif; ?>
<?php endif;
wp_reset_query(); ?>
