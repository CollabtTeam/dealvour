<?php if ( is_active_sidebar( 'search_top' ) ) : ?>
	<div class="ui grid container">
		<div class="sixteen wide column">
			<div class="search-res-block">
				<?php dynamic_sidebar( 'search_top' ); ?>
			</div>
		</div>
	</div>
<?php endif; ?>
