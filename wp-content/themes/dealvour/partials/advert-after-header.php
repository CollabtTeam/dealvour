<!-- After header advert -->
<?php if ( is_active_sidebar( 'after_header' ) ) : ?>
	<div class="advertising-banner">
		<div class="ui container">
			<div class="advert-wrapper"><?php dynamic_sidebar( 'after_header' ); ?></div>
		</div>
	</div>
<?php endif; ?>
<!-- After header advert end -->

<?php get_template_part('partials/newsletter'); ?>
