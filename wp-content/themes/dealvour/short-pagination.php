<div class="short-pagination-wrapper">
    <div class="pagination-controls">
        <?php
        $next_post = get_next_posts_link();
        $prev_post = get_previous_posts_link();
        ?>
        <?php if (!empty($prev_post) && !empty($next_post)): ?>
            <span class="both prev active"><?php previous_posts_link('Prev', 0); ?></span>
            <span class="both next active"><?php next_posts_link('Next', 0); ?></span>
        <?php elseif (!empty($prev_post) || !empty($next_post)): ?>
            <?php if (!empty($prev_post)): ?>
                <span class="prev active"><?php previous_posts_link('Prev', 0); ?></span>
            <?php else: ?>
                <span class="start"><?php _e('Prev', 'html5blank') ?></span>
            <?php endif; ?>

            <?php if (!empty($next_post)): ?>
                <span class="next active"><?php next_posts_link('Next', 0); ?></span>
            <?php else: ?>
                <span class="end"><?php _e('Next', 'html5blank') ?></span>
            <?php endif; ?>
        <?php endif; ?>
    </div>
</div>
