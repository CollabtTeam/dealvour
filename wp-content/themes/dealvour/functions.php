<?php

require_once('core/index.php');

/*------------------------------------*\
	External Modules/Files
\*------------------------------------*/

// Load any external files you have here

/*------------------------------------*\
	Theme Support
\*------------------------------------*/

if (function_exists('add_theme_support')) {
    // Enables post and comment RSS feed links to head
    add_theme_support('automatic-feed-links');

    // Localisation Support
    load_theme_textdomain('html5blank', get_template_directory() . '/languages');
}


function is_expired($_date)
{
    if (!$_date) return false;

    $date = strtotime($_date);
    $today = strtotime(date("Y-m-d"));

    return $today > $date;
}

function acf_the_url($field)
{
    echo $field['url'];
}

function acf_the_url_abbr($field)
{
    if ($field['title']) {
        echo $field['title'];
    } else {
        $_parsed = parse_url($field['url']);
        echo $_parsed['host'];
    }
}

function dv_smart_truncate($text, $length)
{
    $length = abs((int)$length);
    if (strlen($text) > $length) {
        $text = preg_replace("/^(.{1,$length})(\s.*|$)/s", '\\1...', $text);
    }
    echo($text);
}

function the_dv_paginaiton()
{
    global $wp_query;
    $big = 999999999;
    echo paginate_links(array(
        'base' => str_replace($big, '%#%', get_pagenum_link($big)),
        'format' => '?paged=%#%',
        'current' => max(1, get_query_var('paged')),
        'total' => $wp_query->max_num_pages
    ));
}

add_action('after_post_content', 'dv_after_post_content_render_advert', 10, 2);

function dv_after_post_content_render_advert($index, $not_last_post)
{

    if (get_theme_mod('news_advert_repeat')) {
        if ($not_last_post && $index !== (int)get_theme_mod('news_per_page') && $index % (int)get_theme_mod('news_advert_after') === 0) {
            echo get_template_part('partials/advert-news-middle');
        }
    } else {
        if ($not_last_post && $index === (int)get_theme_mod('news_advert_after')) {
            echo get_template_part('partials/advert-news-middle');
        }
    }
}

add_action('after_deal_loop_content', 'dv_after_deal_loop_content', 10, 2);

function dv_after_deal_loop_content($index, $not_last_post)
{

    if (get_theme_mod('deal_advert_repeat')) {
        if ($not_last_post && $index % (int)get_theme_mod('show_advert_deal_after') === 0) {
            echo '</div>';
            echo get_template_part('partials/advert-deals_cat');
            echo '<div class="products-card-wrapper">';
        }
    } else {
        if ($not_last_post && $index === (int)get_theme_mod('show_advert_deal_after')) {
            echo '</div>';
            echo get_template_part('partials/advert-deals_cat');
            echo '<div class="products-card-wrapper">';
        }
    }
}


add_action('after_top_deal_loop_content', 'dv_after_top_deal_loop_content', 10, 2);

function dv_after_top_deal_loop_content($index, $not_last_post)
{

    if (get_theme_mod('top_deal_advert_repeat')) {
        if ($not_last_post && $index % (int)get_theme_mod('show_advert_hot_deal_after') === 0) {
            echo '</div>';
            echo get_template_part('partials/advert-today-deals');
            echo '<div class="products-card-wrapper">';
        }
    } else {
        if ($not_last_post && $index === (int)get_theme_mod('show_advert_hot_deal_after')) {
            echo '</div>';
            echo get_template_part('partials/advert-today-deals');
            echo '<div class="products-card-wrapper">';
        }
    }
}

function is_blog()
{
    return (is_archive() || is_author() || is_category() || is_home() || is_single() || is_tag()) && 'post' == get_post_type();
}

add_action('wp_enqueue_scripts', 'dv_override_plugin_script');

function dv_override_plugin_script()
{


    wp_deregister_script('es-widget-page', ES_URL . 'widget/es-widget-page.js', '', '', true);
    wp_register_script('es-widget-page', get_template_directory_uri() . '/overrides/es-widget-page.js', '', '', true);
    wp_enqueue_script('es-widget-page');
    $es_select_params = array(
        'es_email_notice' => _x('Please enter email address', 'widget-enhanced-select', ES_TDOMAIN),
        'es_incorrect_email' => _x('Please provide a valid email address', 'widget-enhanced-select', ES_TDOMAIN),
        'es_load_more' => _x('loading...', 'widget-enhanced-select', ES_TDOMAIN),
        'es_ajax_error' => _x('Cannot create XMLHTTP instance', 'widget-enhanced-select', ES_TDOMAIN),
        'es_success_message' => _x('Thanks for subscribing! We\'ll deliver the hottest deals straight to your inbox.', 'widget-enhanced-select', ES_TDOMAIN),
        'es_success_notice' => _x('Thanks for subscribing! Please check your email to confirm your subscription. Thank you!', 'widget-enhanced-select', ES_TDOMAIN),
        'es_email_exists' => _x('Email Address already exists!', 'widget-enhanced-select', ES_TDOMAIN),
        'es_error' => _x('Oops.. Unexpected error occurred.', 'widget-enhanced-select', ES_TDOMAIN),
        'es_invalid_email' => _x('Invalid email address', 'widget-enhanced-select', ES_TDOMAIN),
        'es_try_later' => _x('Please try after some time', 'widget-enhanced-select', ES_TDOMAIN),
        'es_problem_request' => _x('There was a problem with the request', 'widget-enhanced-select', ES_TDOMAIN)
    );
    wp_localize_script('es-widget-page', 'es_widget_page_notices', $es_select_params);


}
