<?php /* Template Name: Helpful links Template */ get_header(); ?>

<main>
    <div class="content-wrapper">
        <div class="ui grid container">
            <div class="sixteen wide tablet four wide computer column">
                <div class="left-widget-nav">
                    <h3 class="header-widget"><?php _e('Helpful Links','html5blank')?></h3>
                    <?php $frontend::menu('sidebar-menu', 'widget-nav-list')?>
                </div>
            </div>
            <div class="sixteen wide tablet twelve wide computer column">
                <div class="page-content hide-comment">
                    <h2 class="header-content"><?php the_title(); ?></h2>
                    <?php if (have_posts()): while (have_posts()) : the_post(); ?>

                        <!-- article -->
                        <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

                            <?php the_content(); ?>

                        </article>
                        <!-- /article -->

                    <?php endwhile; ?>

                <?php else: ?>

                    <!-- article -->
                    <article>

                        <h2><?php _e( 'Sorry, nothing to display.', 'html5blank' ); ?></h2>

                    </article>
                    <!-- /article -->

                <?php endif; ?>
            </div>
        </div>
    </div>
</div>
</main>

<?php get_footer(); ?>

