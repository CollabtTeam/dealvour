    <?php get_header(); ?>

    <main role="main">
        <div class="product-content-wrapper">
            <?php get_template_part('partials/advert-after-header') ?>
            <div class="product-news-content">
                <div class="ui grid container">
                    <div class="sixteen wide tablet eleven wide computer column">
                        <section class="article-news hide-comment article-news-wrapper" id="post-<?php the_ID(); ?>">
                            <a href="<?php echo get_permalink(get_option('page_for_posts')) ?>"
                               class="back-buttons"><i
                               class="reply icon"></i><?php _e('Back to Articles Home', 'html5blank'); ?></a>
                               <?php if (have_posts()): while (have_posts()) : the_post(); ?>
                                <a href="javascript:void(0);"><h2 class="title"><?php the_title(); ?></h2></a>
                                <p class="sub-title">
                                    <?php if (get_field('sub_title')) {
                                        the_field('sub_title');
                                    } ?>
                                </p>
                                <div class="news-details">
                                    <div class="description">
                                        <span class="date-news"><?php _e('Published ', 'html5blank'); ?><?php the_time('F j, Y'); ?></span>
                                        <span class="author"><?php _e('By ', 'html5blank'); ?><?php the_author_posts_link(); ?></span>
                                    </div>
                                    <div class="social-buttons">
                                        <?php do_action('render_share_icon'); ?>
                                    </div>
                                </div>
                                <div class="news-content">
                                    <?php if (has_post_thumbnail()) : ?>
                                        <a href="<?php the_permalink(); ?>" title="<?php the_title(); ?>">
                                            <?php the_post_thumbnail('large'); ?>
                                        </a>
                                    <?php endif; ?>
                                    <?php the_content(); ?>
                                </div>
                            <?php endwhile; endif; ?>
                        </section>

                        <?php if (get_field('allow_comment')): ?>
                            <div class="content-wrapper">
                                <section class="page-content">
                                    <div class="comment-wrapper">
                                        <h2 class="title"><?php _e('Leave a comment', 'html5blank') ?></h2>
                                        <?php echo do_shortcode('[wpdevart_facebook_comment curent_url="' . get_permalink() . '" order_type="social" title_text="" title_text_color="transparent" title_text_font_size="0" title_text_font_famely="monospace" title_text_position="left" width="100%" bg_color="#d4d4d4" animation_effect="random" count_of_comments="3" ]'); ?>
                                    </div>
                                </section>
                            </div>
                        <?php endif; ?>

                    </div>
                    <?php get_sidebar(); ?>
                </div>
            </div>
            <?php get_template_part('partials/advert-before-footer') ?>
        </div>

    </main>

    <?php get_footer(); ?>
